import { createApp } from './main'

export default context => {
  return new Promise((resolve,reject) => {
    const { app } = createApp()
    const router = app.$router

    const { url } = context
    const { fullPath } = router.resolve(url).route

    if( fullPath !== url ) {
      return reject({ url:fullPath })
    }

    // 更改路由
    router.push(url)

    router.onReady(()=>{
      // 获取相应路由下的组件
      const matchedComponents = router.getMatchedComponents()

      // 没有路由匹配 返回404
      if(!matchedComponents.length){
        return reject({code:404})
      }

      // resolve(app)

      // 遍历路由下所有的组件，如果有需要服务端渲染的请求，则进行请求
      Promise.all(matchedComponents.map(component=>{
        if(component.serverRequest){
          // 组件中如果有serverRequest对象 判断是否需要服务端请求数屈打成招，并传入一个store参数
          return component.serverRequest(app.$store)
        }
      })).then(()=>{
        context.state = app.$store.state;
        resolve(app)
      }).catch(reject)

    },reject)
  })
}

