const Vue = require('vue')
const server = require('express')()
const renderer = require('vue-server-renderer').createRenderer()

const app = new Vue({
  template:'<div>hello</div>'
})

// 服务器端渲染的核心就在于
// 通过vue-server-renderer插件的renderToString()方法，将vue实例转换为字符串插入到html文件中
server.get('*',(req,res)=>{
  renderer.renderToString(app,(err,html)=>{
    if (err) {
      res.status(500).end('Internal Server Error')
      return
    }
    res.end(`
      <!DOCTYPE html>
      <html lang="en">
        <head><title>Hello</title></head>
        <body>${html}</body>
      </html>
    `)
  })
})
server.listen(9999,()=>{
  console.log("服务器已启动！")
})